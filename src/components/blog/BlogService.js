import config from './../../app.config'
export default (http) => {
  return {
    /**
     * get all posts
     * @returns {*|String|Function|Object}
     */
    getPosts () {
      return http.get(config.resources.posts);
    },

    /**
     * get all posts
     * @returns {*|String|Function|Object}
     */
    getPost (postId) {
      return http.get(config.resources.post(postId));
    },

    getPostComments (postId) {
      return http.get(config.resources.postComments(postId));
    },

    /**
     * get blog section data
     * @returns {*}
     */
    getBlog () {
      return http.get(config.resources.blog);
    },

    /**
     * get all categories
     * @returns {*}
     */
    getBlogCategories () {
      return http.get(config.resources.blogCategories);
    },
    /**
     * get all tags
     * @returns {*}
     */
    getBlogTags () {
      return http.get(config.resources.blogTags);
    }
  }
};
