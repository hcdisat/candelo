import config from '../../app.config'
export default ($http) => {
  let getServices = () => {
    return $http.get(config.resources.services)
      .then(response => {
        return response.data
      });
  };

  let getPages = (page) => {
    let pageUrl = config.resources.pages(page);
    return $http.get(pageUrl).then(
      response => {
        let page = response.data;
        return [
          page.content,
          page.title,
          page.description
        ];
      },
      () => {
        this.content = `
          <div class="alert alert-danger row-margin">
            The page could not load correctly. please try again later.
          </div>
          `;
      }
    );
  };

  let getProducts = () => {
    return $http.get(config.resources.products);
  };

  let getCategories = () => {
    return $http.get(config.resources.productCategories);
  };

  return {
    getServices () {
      return getServices();
    },
    getPages (page) {
      return getPages(page);
    },
    getProducts () {
      return getProducts();
    },
    getCategories () {
      return getCategories();
    }
  }
}
