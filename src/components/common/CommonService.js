import config from './../../app.config'
export default($http) => {
  let retrieveAdds = () => {
    return $http.get(config.resources.adds);
  };

  return {
    getAdds: retrieveAdds
  };
}
