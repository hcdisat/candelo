import config from '../../app.config'
/**
 * @param $http vue-resource
 * @returns {{getSliders: (function()), getSocialMedia: (function()), getRecentPosts: (function())}}
 */
export default ($http) => {
  /**
   * get Sliders
   * @returns {Array}
   */
  let getSliders = () => {
    return $http.get(config.resources.sliders);
  };
  /**
   * Get social media info
   */
  let getSocialMedia = () => {
    return $http.get(config.resources.socialMedia);
  };
  /**
   * Get recent Posts
   */
  let getRecentPosts = () => {
    return $http.get(config.resources.recentPosts);
  };

  return {
    getSliders: getSliders,
    getSocialMedia: getSocialMedia,
    getRecentPosts: getRecentPosts
  }
}
