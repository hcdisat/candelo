import config from './../../app.config'
export default (http) => {
  return {

    /**
     * get all zodiac signs
     * @returns {*}
     */
    getZodiacs () {
      return http.get(config.resources.zodiacs);
    },

    /**
     * zodiacs to main session
     * @returns {*}
     */
    getActualZodiacs () {
      return http.get(config.resources.currentHoroscopes);
    },

    /**
     * all horoscopes
     * @returns {*}
     */
    getHoroscopesSection () {
      return http.get(config.resources.pageHoroscopes);
    },

    /**
     * get single horoscope
     * @returns {*}
     */
    getHoroscope (zodiacId) {
      return http.get(config.resources.horoscope(zodiacId));
    }
  }
}
