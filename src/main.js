import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App';
import routes from './routes';
import init from './bootstrap/init';

init(Vue);
Vue.use(VueRouter);
Vue.http.interceptors.push((request, next) => {
  request.headers['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
  next();
});

export var router = new VueRouter({
  linkActiveClass: 'active',
  hashbang: false,
  history: true,
  mode: 'html5',
  transitionOnLoad: true,
  root: '/'
});
// export var router = new VueRouter({
//   hashbang: false,
//   history: true,
//   mode: 'html5',
//   linkActiveClass: 'active',
//   transitionOnLoad: true,
//   root: '/'
// });
router.map(routes);
router.start(App, '#app');
