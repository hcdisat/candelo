import _ from 'underscore'
export default (() => {
  let config = {
    host: 'http://yerberiaesanmiguel.com',
    port: '',
    prefix: 'api'
  };

  let getResource = name => {
    let port = _.isEmpty(config.port)
      ? ''
      : `:${config.port}`;

    return `${config.host}${port}/${config.prefix}/${name}`;
  };
  return {
    resources: {
      sliders: getResource('sliders'),
      socialMedia: getResource('social-media'),
      recentPosts: getResource('recent-posts'),
      posts: getResource('blog/posts'),
      blog: getResource('pages/blog'),
      blogCategories: getResource('blog/categories'),
      blogTags: getResource('blog/tags'),
      appConfig: getResource('config'),
      services: getResource('services'),
      messages: getResource('messages'),
      products: getResource('products'),
      productCategories: getResource('categories'),
      adds: getResource('commons/adds'),
      currentHoroscopes: getResource('recent-horoscopes'),
      horoscopes: getResource('horoscopes'),
      pageHoroscopes: getResource('page-horoscopes'),
      zodiacs: getResource('zodiacs'),
      pages (name) {
        return getResource(`pages/${name}`);
      },
      post (postId) {
        return getResource(`posts/${postId}`);
      },
      postComments (postId) {
        return getResource(`comments?post_id=${postId}`);
      },
      horoscope (zodiacId) {
        return getResource(`horoscopes?zodiac_id=${zodiacId}`);
      }
    }
  }
})()
