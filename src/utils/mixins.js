import PagesService from './../components/pages/PagesService'
import Loading from './../components/common/Loading'
import Page from './../components/pages/Pages'
export default {
  template: '<page :page="page" :title="title" :description="description" :content="content" ></page>',
  data () {
    return {
      content: '',
      title: '',
      description: ''
    }
  },
  route: {
    data ({ next }) {
      PagesService(this.$http)
        .getPages(this.page)
        .then((content) => {
          next({
            content: content[0],
            title: content[1],
            description: content[2]
          }) });
    }
  },
  components: { Page, Loading }
};
