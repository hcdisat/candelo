import _ from 'underscore'
export default {
  routes: null,
  /**
   * get a route by name
   * @param name
   */
  getRoute (name) {
    this.routes = _.isNull(this.routes)
      ? require('../routes')
      : this.routes;
    let founded = Object.keys(this.routes.default).map(key => {
      let route = this.routes.default[key];
      if (route.name === name) {
        return {
          text: route.text,
          path: route.path,
          multiple: route.multiple,
          links: route.links
        };
      }
    }).filter(r => !_.isUndefined(r));
    return !_.isEmpty(founded) ? founded[0] : null;
  },

  /**
   * Set a timeout to a function just to emulate async calls
   * @param func
   * @param time
   * @returns {*|number}
     */
  delay (func, time) {
    time = _.isUndefined(time) ? 3000 : time;
    return setTimeout(func, time);
  },
  Timer (fn, time) {
    let interval = setInterval(fn, time);

    /**
     * stop
     * @returns {timer}
     */
    this.stop = () => {
      if (interval) {
        clearInterval(interval);
        interval = null;
      }
      return this;
    };

    /**
     * start
     * @returns {timer}
     */
    this.start = () => {
      if (!interval) {
        this.stop();
        interval = setInterval(fn, time);
      }

      return this;
    };

    this.reset = (newTime) => {
      // time = newTime || time;
      this.stop().start();
    }
  }
}
