import components from './components'
import globalComponents from './global_components'
import directives from './directives'
export default (Vue) => {
  // window.$ = window.jQuery = require('jquery');
  // require('bootstrap-sass');

  // register components
  Object.keys(globalComponents).forEach(c => {
    Vue.component(c, globalComponents[c]);
  });

  // register components
  Object.keys(components).forEach(c => {
    if (c.hasOwnProperty('config')) {
      Vue.use(components[c].component, components[c].config);
      return;
    }
    Vue.use(components[c].component);
  });

  // register directives
  Object.keys(directives).forEach(d => {
    directives[d](Vue, d);
  });
}
