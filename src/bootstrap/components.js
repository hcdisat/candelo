import Resource from 'vue-resource'
import UI from 'keen-ui'
export default {
  resource: {
    component: Resource
  },
  keen: {
    component: UI
  }
}
