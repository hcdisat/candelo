import Loading from './../components/common/Loading'
import ButtonBack from './../components/common/GoBack'
export default {
  loading: Loading,
  'back-button': ButtonBack
}
