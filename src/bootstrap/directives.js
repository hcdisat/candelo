import HLoad from './../directives/LoadingDirective'
import InitSlider from './../directives/InitSliderDirective'
export default {
  'hc-load': HLoad,
  'init-slider': InitSlider
}
