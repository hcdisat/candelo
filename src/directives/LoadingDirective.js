import _ from 'underscore'
import $ from 'jquery'
export default (vueInstance, name) => {
  vueInstance.directive(name, {

    update (value) {
      if (_.isUndefined(value)) {
        return;
      }

      value.loadingText = _.isUndefined(value.loadingText)
        ? 'Loading'
        : value.loadingText;

      let animations = {
        spin: 'fa fa-spinner fa-spin fa-fw',
        pulse: 'fa fa-spinner fa-pulse fa-fw',
        refresh: 'fa fa-refresh fa-spin fa-fw',
        circle: 'fa fa-circle-o-notch fa-spin fa-fw'
      };

      var text = '';
      let parent = $(this.el).parent().get(0);
      let action = (className) => {
        $(this.el).removeClass();
        $(this.el).addClass(className);
        return value.isLoading
          ? value.loadingText
          : value.parentText;
      };

      if (value.isLoading) {
        text = action(_.isUndefined(animations[this.arg])
          ? animations.spin
          : animations[this.arg]
        );
      } else {
        text = action(value.defaultClass)
      }

      $(parent).text(' ' + text);
      $(parent).prepend($(this.el))
    }
  });
}
