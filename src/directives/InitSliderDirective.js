import $ from 'jquery'
export default (VueInstance, name) => {
  VueInstance.directive(name, {
    update () {
      // #main-slider
      $(() => {
        $('#main-slider.carousel').carousel({
          interval: 1000
        });
      });

      $('.centered').each(() => {
        $(this).css('margin-top', ($('#main-slider').height() - $(this).height()) / 2);
      });

      $(window).resize(() => {
        $('.centered').each(() => {
          $(this).css('margin-top', ($('#main-slider').height() - $(this).height()) / 2);
        });
      });
    }
  });
}
