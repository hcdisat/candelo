import moment from 'moment'
export default (value, format) => {
  moment.locale('es-do');
  if (format) {
    return moment(value).format(format);
  }

  return moment(value).fromNow();
}
