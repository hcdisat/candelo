import Home from './components/home/Home'
import Services from './components/pages/Services'
import Contact from './components/pages/Contact'
import Products from './components/products/Products'
import Blog from './components/blog/Blog'
import Posts from './components/blog/Posts'
import Post from './components/blog/Post'
import Horoscopes from './components/horoscope/Horoscopes'
import Horoscope from './components/horoscope/Horoscope'
import Zodiacs from './components/horoscope/Zodiacs'
import PagesMix from './utils/mixins'
export default {
  '/': {
    name: 'home',
    component: Home,
    text: 'Home',
    multiple: false,
    links: null
  },
  '/pages/services': {
    name: 'services',
    component: Services,
    text: 'Servicios',
    multiple: false,
    links: null
  },
  '/pages/policy': {
    name: 'policy',
    component: {
      data () {
        return {
          page: 'policy'
        }
      },
      mixins: [PagesMix]
    },
    text: 'Política de Privacidad',
    multiple: false,
    links: false
  },
  '/pages/terms': {
    name: 'terms',
    component: {
      data () {
        return {
          page: 'terms'
        }
      },
      mixins: [PagesMix]
    },
    text: 'Terminos de Uso',
    multiple: false,
    links: false
  },
  '/pages/help': {
    name: 'help',
    component: {
      data () {
        return {
          page: 'help'
        }
      },
      mixins: [PagesMix]
    },
    text: 'Ayuda',
    multiple: false,
    links: false
  },
  '/pages/about-us': {
    name: 'about-us',
    component: {
      data () {
        return {
          page: 'about-us'
        }
      },
      mixins: [PagesMix]
    },
    text: 'Acerca de José',
    multiple: false,
    links: false
  },
  '/pages/contact': {
    component: Contact,
    text: 'Contacto',
    multiple: false,
    links: false
  },
  '/pages/products': {
    component: Products,
    text: 'Productos',
    multiple: false,
    links: false
  },
  '/pages/blog': {
    component: Blog,
    name: 'pages.blog',
    text: 'Blog',
    multiple: false,
    links: false,
    subRoutes: {
      '/': {
        component: Posts
      },
      '/posts/:postId/:seo': {
        name: 'blog.post',
        component: Post
      }
    }
  },
  '/pages/horoscopes': {
    component: Horoscopes,
    name: 'pages.horocopes',
    text: 'Horoscopes',
    multiple: false,
    links: false,
    subRoutes: {
      '/': {
        component: Zodiacs
      },
      '/:zodiacId/:sign': {
        component: Horoscope
      }
    }
  }
}
