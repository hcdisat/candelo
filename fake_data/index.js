var data = {
  "product/categories": [],
  products: [],
  "commons/adds": {},
  "blog/categories": [],
  "blog/tags": [],
  "blog/posts": {},
  "posts": {},
  "comments": {},
  "zodiacs": [],
  "horoscopes": [],
  "recent-horoscopes": {},
};

//sliders
data.sliders = [
  {
    "id": 1,
    "background": {
      "folder": "./assets/slider/",
      "name": "bg4",
      "extension": ".jpg"
    },
    "order": 0,
    "boxed": false,
    "centered": false,
    "title": "LLame Ya! (520)746-4888",
    "content": "",
    "button": {
      "show": false,
      "text": "Learn More",
      "route": "home"
    }
  },
  {
    "id": 2,
    "background": {
      "folder": "./assets/slider/",
      "name": "bg2",
      "extension": ".jpg"
    },
    "order": 1,
    "boxed": false,
    "centered": true,
    "title": "Powerful and Responsive Web Design",
    "content": "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
    "button": {
      "show": true,
      "text": "Check it Out!",
      "route": "home"
    }
  },
  {
    "id": 3,
    "background": {
      "folder": "./assets/slider/",
      "name": "bg1",
      "extension": ".jpg"
    },
    "order": 2,
    "boxed": false,
    "centered": false,
    "title": "Powerful and Responsive Web Design",
    "content": "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
    "button": {
      "show": true,
      "text": "Learn More",
      "route": "home"
    }
  }
];
//social media
data["social-media"] = [
  {
    "name": "twitter",
    "title": "Twitter Marketing",
    "body": "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.",
    "url": "http://localhost:8080"
  },
  {
    "name": "facebook",
    "title": "Facebook Marketing",
    "body": "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.",
    "url": "http://localhost:8080"
  },
  {
    "name": "google-plus",
    "title": "Google Plus Marketing",
    "body": "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.",
    "url": "http://localhost:8080"
  },
  {
    "name": "pinterest",
    "title": "Pinterest Marketing",
    "body": "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.",
    "url": "http://localhost:8080"
  }
];
//services
data["services"] = {
  "freeContent": "<h2 class=\"center\">Custom Content</h2><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda cumque ea, itaque provident quaequam sunt tempore. Dicta facilis labore quod sed unde! Ad alias ex ipsam modi pariatur quas.</div><div>A aliquam corporis dicta dignissimos ducimus eius labore necessitatibus possimus ratione, reiciendisrem velit. Ab ipsum, itaque magnam nobis nulla officiis porro quibusdam ratione reiciendis rem saepetemporibus veniam voluptas.</div><div>A accusantium consequatur corporis cumque, deserunt dicta distinctio dolore dolorem error eum fugitharum illo illum incidunt ipsam laborum nemo odio officia quam quis repellat similique sunt temporibusvitae voluptatem!</div><div>Animi dignissimos earum expedita facere incidunt labore magni quasi quo repellendus rerum. Ad assumendadelectus et facere id illum necessitatibus neque numquam odio perspiciatis porro quia quidem repudiandae,sapiente voluptates!</div><div>Ab ad illum inventore labore molestias nam nesciunt odio, quia ratione temporibus tenetur velit?Accusantium aut, doloremque esse excepturi facilis fugiat, magni maiores nam necessitatibus repellendussuscipit voluptatibus. Odit, tempore.</div>",
  "section": {
    "title": "Nuestros Servicios!",
    "description": "LLama ya (888) 660-2859"
  },
  "intro": "<h2>Aquí le Podemos Ayudar!</h2><p>Rompa esa cadena de daño, Haga Feliz el momento de su vida si el éxito ya está presente Expándalo! Llama a José Cándelo, Psíquico, Mentalista y Vidente con facultad de Origen Natural!</p><p> Llama a José Cándelo, Psíquico, Mentalista y Vidente con facultad de <strong>Origen Natural</strong>!</p>",
  "collection": [
    {
      "chunks": [
        {
          "id": 1,
          "name": "Lectura de Cartas",
          "description": "Usted no tiene que decirle nada a José Cándelo las Cartas hablan por usted, solo tenga confianza."
        },
        {
          "id": 2,
          "name": "¿Problemas de Dinero?",
          "description": "Los Psíquicos Consejeros de José Cándelo serán la Guía para el Progreso!"
        }
      ]
    },
    {
      "chunks": [
        {
          "id": 3,
          "name": "¿Problemas en el Amor?",
          "description": "José Cándelo es consejero y Mentalista con facultades de origen natural, él puede ser una gran guía."
        },
        {
          "id": 4,
          "name": "Mentalista",
          "description": "En Cada Llamada te ayudaremos con los problemas que aquejan tu vida!"
        }
      ]
    }
  ]
};
data["pages/policy"] = {
  "id": 1,
  "title": "Política de Privacidad",
  "description": "Esta Política de Privacidad en Línea y Declaración Legal",
  "content": "<h2>Esta Política de Privacidad en Línea y Declaración Legal aplica a Josecandelo.com y Está orientada a hacer cumplir nuestro compromiso de privacidad y proteccion de su información personal y dejarle saber cómo la usamos. Esta Política solo aplica a actividades en Josecandelo.com y no a otros sitios en que tenemos enlaces. Cuando use nuestro sitio web está aceptando nuestra Política. Si tiene preguntas sobre este Política, por favor póngase en contacto con nosotros a info@Josecandelo.com. </h2> <h3>Información que colectamos sobre usted</h3> <p>Cuando hace un pagoen con nosotros requerimos la siguiente información personal:</p> <ul> <li>Dirección de email</li> <li>Nombre</li> <li>Dirección de facturación</li> <li>Número de teléfono</li> <li>Información de Tarjeta de Crédito</li> </ul> <p>También colectamos y guardamos la siguiente información técnica cuando visita nuestro sitio web:</p> <ul> <li>Dirección de IP</li> <li>Tipo de Navegador</li> <li>Uso de Sitio Web</li> </ul> <p> Guardamos la información de dirección de IP, demográfica, y preferencia de uso que colectamos de usted para mejorar la experiencia de uso, para mejorar la funcionalidad de nuestro sitio web, y para que podemos ponernos en contacto con usted en referencia de su cuenta o transacción.</p> <p>Para ofrecer la mejor experiencia a todos visitantes, Josecandelo.com también puede usar una “Cookie”, que es un pieza de información pequeña guardado por su navegador de web en el disco dura se su computadora. Cookies ayudan a personalizar sus visitas a nuestro sitio web por “recordando” algunas cosas, como sus preferencias. Son específicos al sitio web, así que sus cookies de Josecandelo.com no pueden ser leídos por otros sitios web. Ten por seguro que solamente usamos esta información para uso interna y NO COMPARTIMOS la información con otros.</p> Josecandelo.com también usa Google Alalytics que nos deja rastrear datos Demográficos. También se hace esto usando Cookies. Puede decidir no participar en el rastreo de Google Analíticos en el actualmente disponible sitio web de Google Analíticos específico para gente quien no quiere participar. Usted puede borrar la información guardado en su disco duro en una archiva o carpeta de computador marcado “Cookies” al cualquier momento. La mayoridad de navegadores de web automáticamente acepta cookies, pero puede modificar sus configuraciones para notificarle para que puede decidir a aceptarlos o no. Rechazando cookies va a limitar su habilidad a asesar parte del contenido y/o funcionalidad de este sitio web. No dejamos que otras empresas configuran o asesan cookies en su navegador. Uso de nuestro servicios Jose Candelo nunca divulgará intencionalmente los contenidos de las interacciones entre nuestros miembros y psíquicos. Cuando usa nuestros servicios de teléfono, chat o mensaje usted consiente a la grabacion y/o la llamada privada, los contenidos de estas comunicaciones para su protección o asegurar la calidad. Jose Candelo y comunicaciones de marketing de tercero persona a usted Para informar nuestros clientes de horóscopos, boletines de noticias y ofertas especiales de JoseCandelo.com y/o nuestros socios, podemos enviarle esta información por correo postal, email, o teléfono basado en la información de contacto proveído por el cliente. Detalles de cada tipo de comunicación – y como borrarse de cada listo – se explica abajo. Correo postal– correo postal sobre otros noticias, incluyendo pero limitado a, información sobre su cuenta Marketing de Email – comunicaciones periódicas por email sobre noticias de Psychic Source, información o especial ofertas Enviadas adicionales de JoseCandelo.com – también comunicamos regularmente con usted por teléfono y/o email para proveer cualquier servicios usted pide o espera de Josecandelo.com. Usted recibiera email para notificarle de lo siguiente: confirmación de su cuenta nueva de Josecandelo.com, para confirmar sus transacciones en JoseCandelo.com, para informarle de cambios importantes y mejoras de nuestro servicio y para mandarle noticias u otras revelaciones requeridas por la ley. Porque estos son esenciales a su cuenta y relación con Josecandelo.com. Como ser añadido o quitado de nuestras listas Si desea ser borrado de la lista de comunicaciones de marketing de Josecandelo.com, por favor provee Mandenos un correo electronico a info@josecandelo.com."
};
//terms
data["pages/terms"] = {
  "id": 2,
  "title": "Terminos de Uso",
  "description": "Limitación de responsabilidad",
  "content": "<h3>Introduction</h3> <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> <p>&nbsp;</p> <h3>Unauthorized/Illegal Usage</h3> <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p> <p>&nbsp;</p> <h3>Ownership</h3> <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> <p>&nbsp;</p> <h3>Anti-fraud</h3> <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> <p>&nbsp;</p> <h3>Refund policy</h3> <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed</p> <p>&nbsp;</p> <h3>Warranty and support</h3> <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>"
};
//help
data["pages/help"] = {
  "id": 3,
  "title": "Ayuda",
  "description": "Como usar nuestra pagina web.",
  "content": "<h2>Help page content goes here.</h2> <h4>You can edit it in Admin Panel -> Content -> Pages.</h4>"
};
//about
data["pages/about-us"] = {
  "id": 4,
  "title": "Acerca Nuestro",
  "description": "LLame ya! <strong>(520)746-4888</strong>",
  "content": "<h2>¿Necesita un Guía en su vida?</h2> <h3><small>¿Algo que lo ayude a Salir adelante?</small> </h3> <h3><small>¿Está buscando un consejero con usted sienta plena confianza?</small> </h3> </h2> <p>En el camino de la vida la gente desea realizar una consulta para obtener otra perspectiva o simplemente para tener una conversación familiar sobre ciertos problemas o situaciones, pero les parece un paso demasiado grande ir a ver a una vidente o a un médium. Con José Cándelo se lo queremos poner más fácil: basta con llamar desde su entorno familiar a un consultor experimentado.</p> <p>Don José tiene muchos años de Experiencia en los Estados Unidos, en la ciudad de Tucson, José Cándelo ha ayudado a mucha gente tanto que vienen de otros estados a consulta con José Cándelo. Al llamar a la Línea Espiritual de José Cándelo usted tiene la garantía que lo van a ayudar a resolver sus problemas, José Cándelo tiene un Don o Facultad de origen natural y Puede estar seguro que hará una gran diferencia, No lo dude y llame Ya!</p>"
};
//contact
data["pages/contact"] = {
  "id": 5,
  "title": "Contactanos Con Confianza!",
  "description": "En la Linea Espiritual de Jose Candelo respetamos la privacidad de todos!",
  "content": ""
};
//messages
data["messages"] = [
  {
    "id": "1",
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail",
    "message": "Esto es un mensaje man"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "The Message cono",
    "id": "777969e5-75d2-4191-937d-ef842ee42a14"
  },
  {
    "first_name": "",
    "last_name": "",
    "email": "",
    "message": "",
    "id": "5462bd07-d7ef-4036-bd2b-929969dc78b3"
  },
  {
    "first_name": "",
    "last_name": "",
    "email": "",
    "message": "",
    "id": "3b6e7ae3-bd95-4ae7-8c7a-37ef13095855"
  },
  {
    "first_name": "",
    "last_name": "",
    "email": "",
    "message": "",
    "id": "d7584092-4ea0-4e63-a22a-1f965a99fce4"
  },
  {
    "first_name": "",
    "last_name": "",
    "email": "",
    "message": "",
    "id": "49c45c32-91c2-4371-8124-2b361cd0ce2f"
  },
  {
    "first_name": "Hector Anival",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "MMG",
    "id": "da83f059-3066-4ba8-a3b4-bf9352c9eb86"
  },
  {
    "first_name": "Hector Anibal",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "Nema",
    "id": "8ae604d7-4784-4561-9d0b-434d16bd584b"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "<kdsjdjskfjskdjfm",
    "id": "61f03d59-3664-4953-8e09-8ac7c8094780"
  },
  {
    "first_name": "",
    "last_name": "",
    "email": "",
    "message": "",
    "isSending": false,
    "id": "0ead136a-1b75-4dcf-ab95-0d246e7088c8"
  },
  {
    "first_name": "Anibal",
    "last_name": "Labina",
    "email": "anival66@hotmail.com",
    "message": "Quiero Partirle el Toto, Ayudame!",
    "isSending": false,
    "id": "0825a115-9f44-4557-ab1a-39f59a2a3e96"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "Quiero Partirle el Toto, Ayudame!",
    "id": "5731d747-a342-4f6b-8357-0a41e738bfbe"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "value.loading",
    "id": "74fd38d2-d725-4db0-b4f1-01d0f09a1435"
  },
  {
    "first_name": "",
    "last_name": "",
    "email": "",
    "message": "",
    "id": "7648f9ef-9995-4c32-916f-7b49f89f280f"
  },
  {
    "first_name": "",
    "last_name": "",
    "email": "",
    "message": "",
    "id": "fe1e3f8f-5e1e-489d-ba2a-eb3fdbdcbb4f"
  },
  {
    "first_name": "",
    "last_name": "",
    "email": "",
    "message": "",
    "id": "613ea676-6753-4963-bebb-bf92e38bd56b"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "El mensaje",
    "id": "7bd070d9-a6c4-490b-b0d6-b68bbda35ffe"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "Please",
    "id": "ae9d4ac7-9469-4050-a608-4270f23987b5"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "let animations = {\n        spin:",
    "id": "ffa48621-b641-49d0-b850-0bd99308e570"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "value.isLoading",
    "id": "1a2c8c50-221b-4173-bded-8ec2f89c23cb"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "className",
    "id": "0b128979-1125-4798-8ada-8d2093bbafd9"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "animations[this.arg]",
    "id": "bcb3998f-496a-4036-9d3d-9837b80ad7d3"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "animations[this.arg]",
    "id": "920e0ba3-bcc3-4950-89ee-b47e52842ea0"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "import _ from 'underscore'\nexport default {\n    delay (func, time) {\n        \"use strict\";\n        time = _.isUndefined(time) ? 3000 : time;\n        return setTimeout(func, time);\n    }\n}\n",
    "id": "6e11f4ad-60d8-4308-8946-5b6f9232ac8d"
  },
  {
    "first_name": "Ann",
    "last_name": "Johnson",
    "email": "annj30@gmail.com",
    "message": "Tengo problemas en el amor.",
    "id": "ddff0861-2818-40bd-82b1-7becf44dab28"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "La vaina",
    "id": "6da50858-6ae2-4e58-a789-800b305a76e2"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "Send MessageSend MessageSend Message",
    "id": "0cbb2ed2-6fdb-4fbb-a27c-6927ade085fd"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "UI",
    "id": "b815ed47-2f53-484f-95d9-32234f546ef3"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "Please",
    "id": "ef1f2007-b28e-46e8-880c-3aa210bff99d"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "import UI from 'keen-ui'",
    "id": "38c3969f-f335-4751-bf3c-f135eb8477f4"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "ddddgdfdf",
    "id": "e1fddf59-0eae-4d1f-a757-431193ec8f54"
  },
  {
    "first_name": "Hector",
    "last_name": "Caraballo",
    "email": "hcdisat@gmail.com",
    "message": "dsdfddf",
    "id": "59292622-9f55-488d-a57c-5c1581c5405c"
  }
];

//app config
data["app/config"] = [
  {
    "id": 1,
    "name": "version",
    "value": "0.1-dev",
    "description": "App version, serves to keep a control of the current running instance"
  },
  {
    "id": 2,
    "name": "commit",
    "value": "57337e4",
    "description": "development"
  },
  {
    "id": 3,
    "name": "main_url",
    "value": "/",
    "description": "Main app's url, usually is /."
  },
  {
    "id": 4,
    "name": "route_blacklist",
    "value": "home:policy:terms:help",
    "description": "Routes to be removed form the navbar"
  },
  {
    "id": 5,
    "name": "footer_routes",
    "value": "home:policy:terms:help",
    "description": "Routes to be displayed in the footer"
  },
  {
    "id": 6,
    "name": "app_name",
    "value": "José Candelo",
    "description": "App's name"
  }
];

module.exports = function () {
  var faker = require('faker');
  var _ = require('lodash');
  var moment = require('moment');

  // generate productCategories
  for (var i = 0; i <= 30; i++) {
    data["product/categories"].push({
      id: i + 1,
      name: faker.lorem.word()
    });
  }
  data.products = _.times(200, function (n) {
    return {
      id: n + 1,
      category_id: Math.floor((Math.random() * 30) + 1),
      name: faker.lorem.word(),
      description: faker.lorem.paragraph(),
      price: faker.commerce.price(),
      image: faker.image.image(360, 360, true),
      image_small: faker.image.image(640, 640, true)
    }
  });

  data["commons/adds"] = _.times(4, function (n) {
    return {
      id: n + 1,
      imageAdd: faker.image.image(180, 150, true),
      url: faker.internet.url()
    }
  });

  data["blog/categories"] = _.times(10, function (n) {
    return {
      id: n + 1,
      name: faker.commerce.product(),
      description: faker.lorem.text()
    }
  });

  data["blog/tags"] = _.times(15, function (n) {
    return {
      id: n + 1,
      name: faker.commerce.productMaterial(),
    }
  });

  // comments
  data['comments'] = _.times(25, function (n) {
    var userId = faker.random.number({min: 1, max: 10});
    return {
      id: n + 1,
      comment_id: faker.random.number({min: n + 2, max: 24}),
      user_id: userId,
      post_id: faker.random.number({min: 1, max: 10}),
      content: faker.lorem.paragraph(),
      created_at: faker.date.past(),
      comments: [],
      user: {
        id: userId,
        avatar: faker.image.avatar(),
        name: faker.name.findName(),
        email: faker.internet.email()
      }

    }
  });

  var getPosts = _.times(20, function (n) {
    var catId = faker.random.number({min: 1, max: 10}),
      userId = faker.random.number({min: 1, max: 10}),
      tags = _.times(5, function () {
        return data["blog/tags"][faker.random.number({min: 1, max: 10})];
      });
    var title = faker.lorem.sentence();
    var seo = title.split(' ').join('-');
    return {
      id: n + 1,
      user_id: userId,
      blog_category_id: catId,
      url: '/pages/blog/posts/' + (n + 1) + '/' + seo,
      image: faker.image.image(730, 292, true),
      title: title,
      content: faker.lorem.paragraphs().replace('\n \r', '<br />'),
      created_at: faker.date.past(),
      updated_at: faker.date.recent(),
      comments_count: data["comments"].filter(function(c) { return c.post_id === (n + 1)}).length,
      category: _.find(data["blog/categories"], function (c) {
        return c.id === catId;
      }),
      user: {
        id: userId,
        username: faker.internet.userName(),
        name: faker.name.findName()
      },
      tags: tags
    }
  });

  // blog
  data["pages/blog"] = {
    "id": 66,
    "title": "Blog",
    "description": "Pellentesque habitant morbi tristique senectus et netus et malesuada"
  };

  // posts
  data["blog/posts"] = {
    "rows": 2,
    "posts": getPosts
  };

  data["posts"] = getPosts;

  //recent posts
  //"chops": _.chunk(_.take(data["posts"], 9), 3)
  var chops = _.take(_.cloneDeep(data["posts"]), 12).map(function(p) {
    p.image = faker.image.image(260, 260, true);
    return p;
  });
  chops = _.chunk(chops, 3);
  data["recent-posts"] = {
    "title": "Nova - Corporate site template",
    "id": 1,
    "description": "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
    "chops": chops
  };

  //zodiacs
  var beginDate = moment().date(22).month(11);
  data["zodiacs"] = [
    {
      id: 1,
      name: 'Aries',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-aries-1',
      color: 'aries',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(21).month(3).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(20).month(4).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      id: 2,
      name: 'Tauro',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-taurus-zodiac-symbol-of-bull-head-front',
      color: 'tauro',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(21).month(4).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(21).month(5).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      id: 3,
      name: 'Géminis',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-gemini-zodiac-symbol-of-two-twins-faces',
      color: 'geminis',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(21).month(5).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(20).month(6).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      id: 4,
      name: 'Cáncer',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-cancer-astrological-sign-of-crab-silhouette',
      color: 'cancer',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(21).month(6).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(20).month(7).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      id: 5,
      name: 'Leo',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-leo-lion-head-side',
      color: 'leo',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(21).month(7).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(21).month(8).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      id: 6,
      name: 'Virgo',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-virgo-zodiac-symbol',
      color: 'virgo',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(22).month(8).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(22).month(9).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      id: 7,
      name: 'Libra',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-libra-sign-1',
      color: 'libra',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(23).month(9).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(22).month(10).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      id: 8,
      name: 'Escorpio',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-scorpion-sign-shape',
      color: 'escorpio',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(23).month(10).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(22).month(11).format('YYYY-MM-DD HH:mm:ss')
    },{
      id: 9,
      name: 'Sagitario',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-sagittarius-zodiac-symbol',
      color: 'sagitario',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(23).month(11).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(20).month(12).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      id: 10,
      name: 'Capricornio',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-capricorn-1',
      color: 'capricornio',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(21).month(12).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(19).month(1).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      id: 11,
      name: 'Acuario',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-aquarius-zodiac-sign-symbol-1',
      color: 'acuario',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(20).month(1).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(18).month(2).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      id: 12,
      name: 'Piscis',
      description: faker.lorem.paragraphs(2),
      image: 'flaticon-pisces-zodiac-sign',
      color: 'piscis',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      begin_date: moment().date(19).month(2).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment().date(20).month(3).format('YYYY-MM-DD HH:mm:ss')
    }
  ];
  data["horoscopes"] = data["zodiacs"].map(z => {
    return {
      id: z.id,
      zodiac_id: z.id,
      title: faker.name.findName(),
      content: faker.lorem.paragraphs(5),
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      zodiac: z
    }
  });

  data["recent-horoscopes"] = {
    "title": "Signos",
    "id": 1,
    "description": "Aquí se el signo actual segun la fecha y los dos signos que le siguen",
    "horoscopes": data["zodiacs"]
  };

  data["page-horoscopes"] = {
    "title": "Horóscopo",
    "id": 8,
    "description": "Mantengase al tanto de nuestros Horoscopos"
  };

  return data
};
